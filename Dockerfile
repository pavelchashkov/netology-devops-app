FROM nginx:1.25.5

# Configuration 
ADD conf /etc/nginx
# Content
ADD app /usr/share/nginx/html

EXPOSE 80
